$(document).ready(function(){

	// License and prices modals
	$('.license-trigger, .prices-trigger').fancybox({
		padding : 55,
        wrapCSS: 'license-modal',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0)'
                }
            }
        },
	});


	// Vacancy modal
	$('.vacancy-modal-trigger').fancybox({
		padding : 0,
        wrapCSS: 'vacancy-modal-wrap',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0)'
                }
            }
        },
	});


	// Vacancy modal click trigger on the input type file
	$('.static-form-file-row label').on('click', function(e){
		e.preventDefault();
		$(this).next().trigger('click');
	});


	// Header submenu toggle
    $('.header-menu a').on('click', function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		$('.header-submenu-wrap').toggleClass('visible');
	});	

	$('.submenu-close-btn').on('click', function(e){
		e.preventDefault();		
		$('.header-submenu-wrap').toggleClass('visible');
		if($('.header-menu a').hasClass('active')){
			$('.header-menu a').removeClass('active');
		}
	});	

	$('.adaptive-submenu-toggler').on('click', function(e){
		e.preventDefault();	
		$(this).toggleClass('active');
		$(this).next().slideToggle();
	});	


	$('.tv-program-item-head a').on('click', function(e){
		e.preventDefault();	
		$(this).toggleClass('active');
		$(this).parent().next().slideToggle();
	});	


	// Accordeon
	$('.accordeon-toggle').on('click', function(e){
		e.preventDefault();

		if($(this).hasClass('active')){
			$(this).next().slideUp(function(){
				$(this).prev().removeClass('active').find('a').text('Развернуть');
			});
		}
		else{
			$('.accordeon-toggle.active').next().slideUp(function(){
				$(this).prev().removeClass('active').find('a').text('Развернуть');
			});			
			$(this).next().slideDown(function(){
				$(this).prev().addClass('active').find('a').text('Свернуть');
			});
		}
	});

	// Vacancy toggle
	$('.vacancy-toggle').on('click', function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).parents('.vacancy-item-head').next().slideToggle();
	});


	// Tariffs toggle
	$('.tariffs-toggle').each(function(){
		if($(this).hasClass('active')){
			$(this).find('a').text('Свернуть');
		}
		else{
			$(this).find('a').text('Развернуть');
		}
	});
	
	$('.tariffs-toggle').on('click', function(e){
		e.preventDefault();
		if($(this).hasClass('active')){
			$(this).next().slideUp(function(){
				$(this).prev().removeClass('active').find('a').text('Развернуть');	
			});			
		}
		else{
			$(this).next().slideDown(function(){				
				$(this).prev().addClass('active').find('a').text('Свернуть');		
			});	
			$('.tariffs-toggle.active').next().slideUp(function(){$(this).prev().removeClass('active').find('a').text('Развернуть');});			
		}
	});


	// Tabs
	$('.tab-toggle-btn').on('click', function(e){
		e.preventDefault();
		var tabHref = $(this).attr('href');
		$(this).parents('.tabs-container').find('.tab-toggle-btn').removeClass('active');
		$(this).parents('.tabs-container').find('.tab-item').removeClass('active');
		$(this).addClass('active');		
		$(tabHref).addClass('active');
	});

	// Adaptive Tabs
	if($(window).width() < 768){

		$('.tab-toggle-btn').on('click', function(e){
			e.preventDefault();
			var linkHref = $(this).attr('href');
			$('html,body').animate({scrollTop: $(linkHref).offset().top - 30}, 1000);
		});
	}


	// Input tel mask
	$("input[name='tel']").mask("+9 (999) 999 99 99");


	// Adaptive header menu toggle
	$('.header-adaptive-menu-toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $('body, html').toggleClass('lockscroll');
    });


	// Datepicker 
	if($('#datepicker').length > 0){
		$('#datepicker').datepicker({
			position: 'bottom center',
		});	
	}


    //Google map init
	var map,
		marker;

	function mapinit() {
        var myLatLng = {lat: 55.771404, lng: 37.613692};
        var map = new google.maps.Map(document.getElementById('company-office-map'), {
			center: myLatLng,
			scrollwheel: false,
			zoom: 18
        });
	
        var marker = new google.maps.Marker({
		    position: myLatLng,
		    title: 'Москва, Малый Каретный переулок, дом 7, строение 1',
		});
		marker.setMap(map);

		var content = document.createElement('div');
		content.innerHTML = "Москва, Малый Каретный переулок, дом 7, строение 1";
		var infowindow = new google.maps.InfoWindow({
			content: content
		});

		google.maps.event.addListener(marker, 'click', function() {
		  infowindow.open(map, marker);
		});
    }

    if($('#company-office-map').length > 0){
		google.maps.event.addDomListener(window, 'load', mapinit);
	}
       	    	
});